SHELL := /bin/bash
.DEFAULT_GOAL = welcome

PROJECT_NAME="Trying out chatGPT"

include ./make/output.makefile
include ./make/docs.makefile

.PHONY: welcome banner list help
welcome: banner help

banner:
	@clear
	@echo -e "$(BG_BLUE)$(WHITE) `printf '%-70s' ' '`$(NO_COLOR)" \
     	&& echo -e "$(BG_BLUE)$(WHITE) `printf '%-70s' $(PROJECT_NAME)`$(NO_COLOR)" \
     	&& echo -e "$(BG_BLUE)$(WHITE) `printf '%-70s' ' '`$(NO_COLOR)"

# See https://gist.github.com/klmr/575726c7e05d8780505a#gistcomment-2858004
# sed script explanation: https://gist.github.com/klmr/575726c7e05d8780505a
## Show this help
help:
	@echo "$$(tput bold)Available commands:$$(tput sgr0)"; \
	sed -ne"/^## /{                                                            `# find ## comments` \
		h;s/.*//;:d" -e"H;n;s/^## //;td" -e"s/:.*//;G;s/\\n## /|/;s/\\n/ /g;p; `# ..and join them with their command with pipe as a separator` \
	}" ${MAKEFILE_LIST}                                                        `# use all included makefiles for the sed command` \
     	| LC_ALL='C' sort -f                                                   `# sort the lines` \
     	| awk 'BEGIN {FS="|"}; {printf "$(TEAL)%s$(NO_COLOR)|%s\n", $$1, $$2}' `# colorize the commands (use pipe as field-separator)` \
        | column -t -s '|'                                                      # column-ize the output (use pipe as separator)
	@echo "";

## List all available make commands
list:
	@echo "$(BOLD)Available commands:$(NO_COLOR)";
	@LC_ALL=C $(MAKE) -pRrq -f $(MAKEFILE_LIST) -f ./makefile : 2>/dev/null \
	| awk -v RS= -F: '{if ($$1 !~ "^[#.]") {print $$1}}' \
	| sort
