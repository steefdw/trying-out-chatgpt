# setaf colors, see `man terminfo | grep -A 12 -B 1 'the RGB values'`
# Color   | setaf/setab |
# ========|=============|
# black   | 0           |
# red     | 1           |
# green   | 2           |
# yellow  | 3           |
# blue    | 4           |
# magenta | 5           |
# cyan    | 6           |
# white   | 7           |
BLACK=$(shell tput setaf 0)
RED=$(shell tput setaf 1)
GREEN=$(shell tput setaf 2)
YELLOW=$(shell tput setaf 3)
BLUE=$(shell tput setaf 4)
PURPLE=$(shell tput setaf 5)
TEAL=$(shell tput setaf 6)
WHITE=$(shell tput setaf 7)
BG_RED=$(shell tput setab 1)
BG_GREEN=$(shell tput setab 2)
BG_YELLOW=$(shell tput setab 3)
BG_BLUE=$(shell tput setab 4)
BG_PURPLE=$(shell tput setab 5)
BG_TEAL=$(shell tput setab 6)
BG_WHITE=$(shell tput setab 7)
BOLD=$(shell tput bold)
NO_COLOR=$(shell tput sgr0)

msg_error = echo -e "$(BG_RED)$(WHITE)⚠️  `printf '%-52s' $(1)`$(NO_COLOR)"
msg_warn = echo -e "$(BG_YELLOW)$(RED)⚠️  `printf '%-52s' $(1)`$(NO_COLOR)"
msg_fix = echo -e "$(BG_YELLOW)$(BLACK)🔨 `printf '%-52s' $(1)`$(NO_COLOR)"
msg_validate = echo -e "$(BG_TEAL)$(BLACK)🔍 `printf '%-52s' $(1)`$(NO_COLOR)"
msg_success = echo -e "$(GREEN)$(BOLD)✅ `printf '%-52s' $(1)`$(NO_COLOR)"
msg_info = echo -e "$(BG_BLUE)$(WHITE)💡 `printf '%-52s' $(1)`$(NO_COLOR)"
msg_docker = echo -e "$(BG_PURPLE)$(WHITE)🐳 `printf '%-52s' $(1)`$(NO_COLOR)"
msg_go = echo -e "$(BG_BLUE)$(WHITE) `printf '%-54s' ' '`$(NO_COLOR)" \
	&& echo -e "$(BG_BLUE)$(WHITE)🚀 `printf '%-52s' $(1)`$(NO_COLOR)" \
	&& echo -e "$(BG_BLUE)$(WHITE) `printf '%-54s' ' '`$(NO_COLOR)"